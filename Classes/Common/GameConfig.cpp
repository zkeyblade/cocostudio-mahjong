//
// Created by farmer on 2018/7/8.
//

#include "GameConfig.h"

static GameConfig* m_pGameConfig = NULL;

GameConfig *GameConfig::getInstance() {     //单例创建全局唯一的GameConfig
    if (m_pGameConfig == NULL){
        m_pGameConfig = new GameConfig();
    }
    return m_pGameConfig;
}

GameConfig::GameConfig() {
    m_EffectsVolume = 0;
    loadConfig();
}

GameConfig::~GameConfig() {

}

/**
 * 加载配置
 */
void GameConfig::loadConfig() {
    m_EffectsVolume = UserDefault::getInstance()->getFloatForKey("EffectsVolume", 0.8f);
}

/**
 * 保存配置
 */
void GameConfig::saveConfig() {
    UserDefault::getInstance()->setFloatForKey("EffectsVolume", m_EffectsVolume);
}

void GameConfig::resetscores() 
{
	std::vector<int64_t> scores;
	for (int i = 0; i < 4; i++)
	{
		scores.push_back(GAMECFG_STARTSCORE);
	}
	savescores(scores);
}

std::vector<int64_t> GameConfig::loadscores() {
	using namespace std;
	vector<int64_t> scores;
	scores.push_back(UserDefault::getInstance()->getIntegerForKey(GAMECFG_PLAYERSCORE, GAMECFG_STARTSCORE));
	scores.push_back(UserDefault::getInstance()->getIntegerForKey(GAMECFG_AI1SCORE, GAMECFG_STARTSCORE));
	scores.push_back(UserDefault::getInstance()->getIntegerForKey(GAMECFG_AI2SCORE, GAMECFG_STARTSCORE));
	scores.push_back(UserDefault::getInstance()->getIntegerForKey(GAMECFG_AI3SCORE, GAMECFG_STARTSCORE));

	return scores;
}

void GameConfig::savescores(std::vector<int64_t> scores) {
	int c = scores.size();
	if (c != 4)
	{
		log("保存分数失败, 只能保存4个分数, 但得到%d个分数", c);
		return;
	}
	UserDefault::getInstance()->setIntegerForKey(GAMECFG_PLAYERSCORE, scores[0]);
	UserDefault::getInstance()->setIntegerForKey(GAMECFG_AI1SCORE, scores[1]);
	UserDefault::getInstance()->setIntegerForKey(GAMECFG_AI2SCORE, scores[2]);
	UserDefault::getInstance()->setIntegerForKey(GAMECFG_AI3SCORE, scores[3]);
}
