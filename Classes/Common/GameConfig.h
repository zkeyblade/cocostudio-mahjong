//
// Created by farmer on 2018/7/8.
//

#ifndef COCOSTUDIO_MAHJONG_GAMECONFIG_H
#define COCOSTUDIO_MAHJONG_GAMECONFIG_H

#include "cocos2d.h"

#define GAMECFG_PLAYERSCORE "playerscore"
#define GAMECFG_AI1SCORE "ai1score"
#define GAMECFG_AI2SCORE "ai2score"
#define GAMECFG_AI3SCORE "ai3score"
#define GAMECFG_STARTSCORE 1000000

using namespace cocos2d;

class GameConfig {

public: 
    float m_EffectsVolume;  //音量

private:
    GameConfig();
    ~GameConfig();

public:
    void loadConfig();  //重新加载配置
    void saveConfig();  //保存配置

	// 分数处理 只支持1人类玩家+3个AI玩家 index 0的值是人类玩家分数
	void resetscores(); // 重置所有人分数
	std::vector<int64_t> loadscores();  //载入分数
	void savescores(std::vector<int64_t> scores);  //保存分数

public:
    /**
     * 获取游戏配置单例
     * @return
     */
    static GameConfig *getInstance();
};


#endif //COCOSTUDIO_MAHJONG_GAMECONFIG_H
