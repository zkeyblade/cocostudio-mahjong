﻿#include "Utility.h"

#include "Common.h"
#include "platform/android/jni/JniHelper.h"

void restartapp()
{
	JniMethodInfo method;

	bool b = cocos2d::JniHelper::getStaticMethodInfo(method, "org.cocos2dx.cpp.AppActivity", "restartapp", "()V");
	if (b)
	{
		method.env->CallStaticVoidMethod(method.classID, method.methodID);
		method.env->DeleteLocalRef(method.classID);
	}
}

timeval appstarttime = { 0, 0 };
void setappstarttime()
{
	gettimeofday(&appstarttime, NULL);
	log("setappstarttime: %ld", appstarttime.tv_sec);
}

long getapprunningtime()
{
	if (appstarttime.tv_sec == 0)
	{
		return 0;
	}

	timeval nowtime;
	gettimeofday(&nowtime, NULL);
	return nowtime.tv_sec - appstarttime.tv_sec;
}

bool isappneedrestart(long apprestarttime)
{
	long runningtime = getapprunningtime();
	if (runningtime > apprestarttime)
	{
		return true;
	}
	return false;
}